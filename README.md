# goff-metadata

A Go library to extract metadata from audio files.

**This library uses CGo with libavformat from FFmpeg to extract.**

[GoDoc](https://gitlab.com/diamondburned/goff-metadata)

## The Free Software Song

This package comes with a copy of the Free Software Song encoded with libopus at 32kbps. This song is used as the test data for `metadata_test.go`. This song is not made by me. All rights reserved. [Source](https://www.gnu.org/music/free-software-song.en.html).

