module gitlab.com/diamondburned/goff-metadata

go 1.12

require (
	github.com/go-test/deep v1.0.1
	github.com/imkira/go-libav v0.0.0-20190125075901-6bf952df9de5
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-colorable v0.1.2 // indirect
)
