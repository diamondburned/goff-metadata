package metadata

import (
	"testing"

	"github.com/go-test/deep"
)

var freeSoftwareSongMetadata = &Metadata{
	Raw: []map[string]string{
		nil,
		map[string]string{
			"ARTIST": "Mark Forry, Yvette Osborne, Ron Fox, Steve Finney," +
				" Bill Cope, Kip McAtee, Ernie Provencher, Dan Auvil",
			"DATE":    "2009",
			"comment": "http://www.gnu.org/music/free-software-song.html",
			"TITLE":   "Free Software Song",
			"GENRE":   "Ethnic",
			"LICENSE": "cc-by-sa",
			"ENCODER": "Lavc58.35.100 libopus",
		},
	},
	Album: "",
	Artist: "Mark Forry, Yvette Osborne, Ron Fox, Steve Finney," +
		" Bill Cope, Kip McAtee, Ernie Provencher, Dan Auvil",
	Title:       "Free Software Song",
	Track:       "",
	Date:        "2009",
	AlbumArtist: "",
	Disc:        "",
	Composer:    "",
	Genre:       "Ethnic",
}

func TestFreeSoftwareSongMetadata(t *testing.T) {
	m, err := ParseFile("_test/FreeSWSongLow.ogg")
	if err != nil {
		t.Fatal(err)
	}

	if diff := deep.Equal(m, freeSoftwareSongMetadata); diff != nil {
		t.Error(diff)
	}
}
