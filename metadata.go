// Package metadata provides ParseFile to extract metadata from audio files
// using FFmpeg and CGo.
package metadata

import (
	"errors"
	"strings"

	"github.com/imkira/go-libav/avformat"
)

const (
	album       = "ALBUM"
	artist      = "ARTIST"
	title       = "TITLE"
	track       = "TRACK"
	date        = "DATE"
	albumArtist = "ALBUM_ARTIST"
	disc        = "DISC"
	composer    = "COMPOSER"
	genre       = "GENRE"
)

// Metadata contains the metadata
type Metadata struct {
	// 1:1 mapping, 0 is always global, streams start at 1.
	// Map could be nil.
	Raw []map[string]string

	Album       string
	Artist      string
	Title       string
	Track       string
	Date        string
	AlbumArtist string
	Disc        string
	Composer    string
	Genre       string
}

// ErrNoMetadata is returned when no metadata are found. This happens when
// all the metadata maps are either nil or zero length.
var ErrNoMetadata = errors.New("no metadata found")

// ParseFile calls FFmpeg to parse the file and return the metadata.
func ParseFile(path string) (*Metadata, error) {
	c, err := avformat.NewContextForInput()
	if err != nil {
		return nil, err
	}

	defer c.Free()

	if err := c.OpenInput(path, nil, nil); err != nil {
		return nil, err
	}

	defer c.CloseInput()

	streams := c.Streams()

	metadata := &Metadata{
		Raw: make([]map[string]string, 0, len(streams)+1),
	}

	metadata.Raw = append(metadata.Raw, c.MetaData().Map())
	for _, s := range streams {
		metadata.Raw = append(metadata.Raw, s.MetaData().Map())
	}

	for _, raw := range metadata.Raw {
		if raw != nil || len(raw) > 0 {
			goto Pass
		}
	}

	return nil, ErrNoMetadata

Pass:
	metadata.parseFields(metadata.transformRaw())

	return metadata, nil
}

func (m *Metadata) transformRaw() map[string]string {
	metaMap := map[string]string{}

	for _, raw := range m.Raw {
		for k, v := range raw {
			metaMap[strings.ToUpper(k)] = v
		}
	}

	return metaMap
}

func (m *Metadata) parseFields(meta map[string]string) {
	m.Album = meta[album]
	m.Artist = meta[artist]
	m.Title = meta[title]
	m.Track = meta[track]
	m.Date = meta[date]
	m.AlbumArtist = meta[albumArtist]
	m.Disc = meta[disc]
	m.Composer = meta[composer]
	m.Genre = meta[genre]
}
